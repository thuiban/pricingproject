<?php

namespace App\Repository;

use App\Entity\CompetitingOffer;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CompetitingOffer|null find($id, $lockMode = null, $lockVersion = null)
 * @method CompetitingOffer|null findOneBy(array $criteria, array $orderBy = null)
 * @method CompetitingOffer[]    findAll()
 * @method CompetitingOffer[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CompetitingOfferRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CompetitingOffer::class);
    }

    // /**
    //  * @return CompetitingOffer[] Returns an array of CompetitingOffer objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CompetitingOffer
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
