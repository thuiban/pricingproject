<?php

namespace App\Repository;

use App\Entity\CompetingOffer;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CompetingOffer|null find($id, $lockMode = null, $lockVersion = null)
 * @method CompetingOffer|null findOneBy(array $criteria, array $orderBy = null)
 * @method CompetingOffer[]    findAll()
 * @method CompetingOffer[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CompetingOfferRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CompetingOffer::class);
    }

    // /**
    //  * @return CompetingOffer[] Returns an array of CompetingOffer objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CompetingOffer
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
