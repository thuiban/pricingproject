<?php

namespace App\Repository;

use App\Entity\OwnOffer;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method OwnOffer|null find($id, $lockMode = null, $lockVersion = null)
 * @method OwnOffer|null findOneBy(array $criteria, array $orderBy = null)
 * @method OwnOffer[]    findAll()
 * @method OwnOffer[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OwnOfferRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, OwnOffer::class);
    }

    // /**
    //  * @return OwnOffer[] Returns an array of OwnOffer objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('o.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?OwnOffer
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
