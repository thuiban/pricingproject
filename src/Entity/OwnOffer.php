<?php

namespace App\Entity;

use App\Entity\Interfaces\OfferInterface;
use App\Repository\OwnOfferRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @ORM\Entity(repositoryClass=OwnOfferRepository::class)
 */
class OwnOffer implements OfferInterface
{

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Product::class, inversedBy="offers")
     */
    private $product;

    /**
     * @ORM\Column(type="money", precision=7, scale=2)
     *
     */
    private $price;


    /**
     * @ORM\Column(type="money", precision=7, scale=2)
     */
    private $minimumPrice;

    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank
     */
    private $state;


    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }


    /**
     * @return Product|null
     */
    public function getProduct(): ?Product
    {
        return $this->product;
    }


    /**
     * @param Product|null $product
     * @return $this
     */
    public function setProduct(?Product $product): self
    {
        $this->product = $product;

        return $this;
    }


    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }


    /**
     * @param  $price
     * @return void
     */
    public function setPrice($price): void
    {
        $this->price = $price;
    }

    /**
     * @return int|null
     */
    public function getState(): ?int
    {
        return $this->state;
    }

    /**
     * @param int $state
     * @return void
     */
    public function setState(int $state): void
    {
        $this->state = $state;
    }

    /**
     * @return mixed
     */
    public function getMinimumPrice()
    {
        return $this->minimumPrice;
    }

    /**
     * @param mixed $minimumPrice
     *
     * @return void
     */
    public function setMinimumPrice($minimumPrice): void
    {
        $this->minimumPrice = $minimumPrice;
    }
}
