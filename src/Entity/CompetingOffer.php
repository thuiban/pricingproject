<?php

namespace App\Entity;

use App\Entity\Interfaces\OfferInterface;
use App\Repository\CompetingOfferRepository;
use Doctrine\ORM\Mapping as ORM;
use Money\Money;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=CompetingOfferRepository::class)
 */
class CompetingOffer implements OfferInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;


    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank
     */
    private $sellerName;

    /**
     * @ORM\Column(type="money", precision=7, scale=2)
     * @Assert\NegativeOrZero
     */
    private $price;

    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank
     */
    private $state;

    /**
     * @ORM\ManyToOne(targetEntity=Product::class, inversedBy="competingOffers")
     */
    private $product;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Product|null
     */
    public function getProduct(): ?Product
    {
        return $this->product;
    }

    /**
     * @param Product|null $product
     * @return $this
     */
    public function setProduct(?Product $product): self
    {
        $this->product = $product;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getSellerName(): ?string
    {
        return $this->sellerName;
    }

    /**
     * @param string $sellerName
     * @return $this
     */
    public function setSellerName(string $sellerName): self
    {
        $this->sellerName = $sellerName;

        return $this;
    }

    /**
     * @return Money
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param  $price
     * @return void
     */
    public function setPrice($price): void
    {
        $this->price = $price;
    }

    /**
     * @return int|null
     */
    public function getState(): ?int
    {
        return $this->state;
    }

    /**
     * @param mixed $state
     * @return void
     */
    public function setState($state): void
    {
        $this->state = $state;
    }
}
