<?php

namespace App\Entity;

use App\Repository\ProductRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;



/**
 * @ORM\Entity(repositoryClass=ProductRepository::class)
 */
class Product
{
    const AVERAGE_STATE = 1;
    const GOOD_STATE = 2;
    const VERY_GOOD_STATE = 3;
    const LIKE_NEW_STATE = 4;
    const NEW_STATE = 5;


    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank
     */
    private $name;


    /**
     * @ORM\OneToMany(targetEntity=OwnOffer::class, mappedBy="product")
     */
    private $offers;

    /**
     * @ORM\OneToMany(targetEntity=CompetingOffer::class, mappedBy="product")
     */
    private $competingOffers;


    public function __construct()
    {
        $this->offers = new ArrayCollection();
        $this->competingOffers = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }


    /**
     * @return Collection|OwnOffer[]
     */
    public function getOffers(): Collection
    {
        return $this->offers;
    }

    public function addOffer(OwnOffer $offer): self
    {
        if (!$this->offers->contains($offer)) {
            $this->offers[] = $offer;
            $offer->setProduct($this);
        }

        return $this;
    }

    public function removeOffer(OwnOffer $offer): self
    {
        if ($this->offers->contains($offer)) {
            $this->offers->removeElement($offer);
            // set the owning side to null (unless already changed)
            if ($offer->getProduct() === $this) {
                $offer->setProduct(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|CompetingOffer[]
     */
    public function getCompetingOffers(): Collection
    {
        return $this->competingOffers;
    }

    public function addCompetingOffer(CompetingOffer $competingOffer): self
    {
        if (!$this->competingOffers->contains($competingOffer)) {
            $this->competingOffers[] = $competingOffer;
            $competingOffer->setProduct($this);
        }

        return $this;
    }

    public function removeCompetingOffer(CompetingOffer $competingOffer): self
    {
        if ($this->competingOffers->contains($competingOffer)) {
            $this->competingOffers->removeElement($competingOffer);
            // set the owning side to null (unless already changed)
            if ($competingOffer->getProduct() === $this) {
                $competingOffer->setProduct(null);
            }
        }

        return $this;
    }

}
