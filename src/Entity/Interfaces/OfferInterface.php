<?php


namespace App\Entity\Interfaces;

/**
 * Interface OfferInterface
 * @package App\Entity
 */
interface OfferInterface
{

    /**
     * @param int $state
     * @return void
     */
    public function setState(int $state): void;

    /**
     * @return int|null
     */
    public function getState(): ?int;

    /**
     * @param  $price
     * @return void
     */
    public function setPrice($price): void;


    /**
     * @return mixed
     */
    public function getPrice();


}