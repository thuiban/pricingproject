<?php


namespace App\Utils;


use App\Entity\OwnOffer;
use App\Entity\OfferInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Money\Money;

class RatioStatePrice
{

    /**
     * @param $offers
     * @return ArrayCollection|null
     * @throws \Exception
     */
    public static function ownOffersSorting($offers)
    {
        return self::ratioOffers($offers);
    }

    /**
     * @param $offers
     * @return ArrayCollection|null
     * @throws \Exception
     */
    public static function competingOfferSorting($offers)
    {
        return self::ratioOffers($offers);
    }

    /**
     * @param $offers
     * @return ArrayCollection|null
     * @throws \Exception
     */
    private static function ratioOffers($offers)
    {
        if (!$offers instanceof  Collection)
        {
            return NULL;
        }

        $iteratorState = $offers->getIterator();
        $iteratorState->uasort(function ($currentOffer, $nextOffer) {
            return ($currentOffer->getState() < $nextOffer->getState()) ? 1 : -1;
        });

        $tmp = new ArrayCollection(iterator_to_array($iteratorState));
        $iteratorPrice = $tmp->getIterator();
        $iteratorPrice->uasort(function ($currentOffer, $nextOffer) {
            if ($currentOffer->getState() === $nextOffer->getState()) {
                return ($currentOffer->getPrice() < $nextOffer->getPrice()) ? -1 : 1;
            } else if ($currentOffer->getState() > $nextOffer->getState()) {
                return ($currentOffer->getPrice()->subtract(Money::EUR(100)) <= $nextOffer->getPrice()) ? -1 : 1;
            }
            return 0;
        });

        return new ArrayCollection(iterator_to_array($iteratorPrice));
    }
}