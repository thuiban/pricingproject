<?php

namespace App\Form;

use App\Entity\OwnOffer;
use App\Entity\Product;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;

class OfferType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('state', ChoiceType::class, [
                    'choices' => array(
                        'product.state.average_state' => Product::AVERAGE_STATE,
                        'product.state.good_state' => Product::GOOD_STATE,
                        'product.state.very_good_state' => Product::VERY_GOOD_STATE,
                        'product.state.like_new_state' => Product::LIKE_NEW_STATE,
                        'product.state.new_state' => Product::NEW_STATE
                    ),
                'choice_translation_domain' => 'messages',
                'label' => 'addOffer.form.state_title',
                'required' => true,
                'attr' => array(
                    'class' => 'form-control'
                ),
                'placeholder' => 'addOffer.form.placeholder_state',
                ])
            ->add('minimumPrice', MoneyType::class, [
                'label' => 'addOffer.form.minimum_price',
                'required' => true,
                'attr' => array(
                    'class' => 'form-control'
                )

            ])
            ->add('price', MoneyType::class, [
                'label' => 'addOffer.form.price',
                'translation_domain' => 'messages',
                'required' => true,
                'attr' => array(
                    'class' => 'form-control' ,
                    'readonly' => true
                ),
                'help' => 'addOffer.form.help_price'
            ])
            ->add('save', SubmitType::class, [
                'label' => 'addOffer.form.add',
                'attr' => array(
                    'class' => 'btn btn-primary'
                )
            ])

        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => OwnOffer::class,
        ]);
    }
}
