<?php


namespace App\Helper;


use App\Entity\Product;

class ProductHelper
{
    /**
     * @param $value
     * @return string|null
     */
    public static function stateToString($value)
    {
        switch ($value) {
            case Product::AVERAGE_STATE:
                return 'product.state.average_state';

            case Product::GOOD_STATE:
                return 'product.state.good_state';

            case Product::VERY_GOOD_STATE:
                return 'product.state.very_good_state';

            case Product::LIKE_NEW_STATE:
                return 'product.state.like_new_state';

            case Product::NEW_STATE:
                return 'product.state.new_state';

            default:
                return NULL;
        }
    }
}