<?php


namespace App\Helper;


use Money\Money;

class MoneyHelper
{

    /**
     * @param $price double
     *
     * @return Money
     */
    public static function createMoneyObject($price)
    {
        if ($price > 0)
        {
            $convertPrice = $price * 100;
            return Money::EUR($convertPrice);
        }

        return null;
    }

}