<?php

namespace App\Twig;

use App\Helper\ProductHelper;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

class ProductExtension extends AbstractExtension
{

    /**
     * @return array|TwigFunction[]
     */
    public function getFilters(): array
    {
        return [
            new TwigFilter('product_state', [$this, 'convertStateToString']),
        ];
    }

    /**
     * @param $value
     * @return string|null
     */
    public function convertStateToString($value)
    {
        return ProductHelper::stateToString($value);
    }
}
