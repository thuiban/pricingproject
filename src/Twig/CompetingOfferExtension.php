<?php

namespace App\Twig;

use App\Utils\RatioStatePrice;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class CompetingOfferExtension extends AbstractExtension
{
    public function getFunctions(): array
    {
        return [
            new TwigFunction('competingOfferSorting', [$this, 'competingOfferSorting']),
        ];
    }

    public function competingOfferSorting($value)
    {
        return RatioStatePrice::competingOfferSorting($value);
    }
}
