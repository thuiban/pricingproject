<?php

namespace App\Controller;

use App\Entity\OwnOffer;
use App\Entity\Product;
use App\Form\OfferType;
use App\Helper\MoneyHelper;
use App\Services\OfferService;
use Doctrine\ORM\EntityManagerInterface;
use Money\Money;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

class IndexController extends AbstractController
{
    /**
     * @var  EntityManagerInterface
     */
    private $em;

    /**
     * @var  TranslatorInterface
     */
    private $translator;

    /**
     * @var OfferService
     */
    private $offerService;

    /**
     * IndexController constructor.
     * @param EntityManagerInterface $em
     * @param TranslatorInterface $translator
     * @param OfferService $offerService
     */
    public function __construct(
        EntityManagerInterface $em,
        TranslatorInterface $translator,
        OfferService $offerService
    ) {
        $this->em = $em;
        $this->translator = $translator;
        $this->offerService = $offerService;

    }


    /**
     * @Route("/", name="index")
     *
     * @return Response
     */
    public function index()
    {

        $productRespository = $this->em->getRepository(Product::class);


        return $this->render(
            'index/index.html.twig',
            [
                'controller_name' => 'IndexController',
                'products' => $productRespository->findAll(),
            ]
        );
    }

    /**
     * @Route("/add-offer/{idProduct}", name="add_offer")
     * @param Request $request
     * @return RedirectResponse|Response
     * @throws \Exception
     */
    public function addOfferAction(Request $request)
    {
        $idProduct = $request->get('idProduct');
        $price = null;
        /**
         * @var Product $product
         */
        $product = $this->em->getRepository(Product::class)->find($idProduct);


        if (is_null($product)) {
            $this->addFlash('warning', $this->translator->trans('addOffer.error.product_not_found'));

            return $this->redirectToRoute('index');
        }

        $newOffer = new OwnOffer();
        $form = $this->createForm(OfferType::class, $newOffer);


        $form->handleRequest($request);
        if ($form->isSubmitted())  {

            $price = $this->offerService->quotePrice(
                (int)$form->get('state')->getData(),
                (float)$form->get('minimumPrice')->getData(),
                $product
            );

            if ($price->equals(Money::EUR(0)))
            {
                $form->addError(new FormError($this->translator->trans('addOffer.error.error_minimum_price')));
            }

            if ($form->isValid())
            {
                $newOffer->setProduct($product);
                $newOffer->setMinimumPrice(MoneyHelper::createMoneyObject((float)$form->get('minimumPrice')->getData()));
                $newOffer->setPrice($price);
                $this->em->persist($newOffer);
                $this->em->flush();
                $this->addFlash('success', $this->translator->trans('addOffer.messages.success'));
                return $this->redirectToRoute('index');

            }
        }

        return $this->render(
            'offer/add-form.html.twig',
            [
                'product' => $product,
                'form' =>$form->createView()
            ]
        );
    }

    /**
     * @Route("/delete-offer/{idOffer}", name="delete_offer")
     * @param Request $request
     * @return RedirectResponse|Response
     * @throws \Exception
     */
    public function deleteOfferAction(Request $request)
    {
        $idOffer = $request->get('idOffer');
        /**
         * @var Product $product
         */
        $offer = $this->em->getRepository(OwnOffer::class)->find($idOffer);

        if (is_null($offer))
        {
            $this->addFlash('error', $this->translator->trans('deleteOffer.error.offer_not_found'));
            return $this->redirectToRoute('index');
        }

        $this->em->remove($offer);
        $this->em->flush();
        $this->addFlash('success', $this->translator->trans('deleteOffer.message.delete_success'));
        return $this->redirectToRoute('index');
    }


}
