<?php

namespace App\DataFixtures;

use App\Entity\CompetingOffer;
use App\Entity\Product;
use App\Helper\MoneyHelper;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {

        $competingsOffers = json_decode('{"data":[{"price":"14.10","name":"Abc jeux","state":1},{"price":"16.20","name":"Games-planete","state":1},{"price":"18","name":"Media-games","state":2},{"price":"20","name":"Micro-jeux","state":3},{"price":"21.50","name":"Top-Jeux-video","state":3},{"price":"24.44","name":"Tous-les-jeux","state":2},{"price":"29","name":"Diffusion-133","state":4},{"price":"30.99","name":"France-video","state":5}]}');

        $product = new Product();
        $product->setName("Kerbal Space Program - Steam Edition");
        $manager->persist($product);
        $manager->flush();

        foreach ($competingsOffers->data as $datum)
        {
            $competingsOffer = new CompetingOffer();
            $competingsOffer->setProduct($product);
            $competingsOffer->setPrice(MoneyHelper::createMoneyObject(floatval($datum->price)));
            $competingsOffer->setState(intval($datum->state));
            $competingsOffer->setSellerName($datum->name);
            $manager->persist($competingsOffer);
        }

        $manager->flush();
    }
}
