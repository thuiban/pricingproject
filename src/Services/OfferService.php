<?php


namespace App\Services;


use App\Entity\Product;
use App\Entity\CompetingOffer;
use App\Helper\MoneyHelper;
use App\Utils\RatioStatePrice;
use Doctrine\Common\Collections\Collection;
use Money\Money;
use Psr\Log\LoggerInterface;


class OfferService
{
    /**
     * @param $state int
     * @param $minimumPrice float
     * @param $product Product
     * @return Money
     * @throws \Exception
     */
    public function quotePrice($state, $minimumPrice, $product)
    {
        $sortCompetingOffers = RatioStatePrice::competingOfferSorting($product->getCompetingOffers());
        $strategyPriceOne = $this->getStrategyPriceOne($sortCompetingOffers, $state);
        $strategyPriceTwo = $this->getStrategyPriceTwo($sortCompetingOffers, $state);
        $minimumPriceObject = MoneyHelper::createMoneyObject($minimumPrice);


        if (!is_null($strategyPriceOne) && $minimumPriceObject->lessThan($strategyPriceOne)) {
            return $strategyPriceOne;
        } else if (!is_null($strategyPriceTwo) && $minimumPriceObject->lessThan($strategyPriceTwo)) {
                return $strategyPriceTwo;
        } else if (is_null($strategyPriceTwo) && is_null($strategyPriceOne)) {
                    return $minimumPriceObject;
        }

        return Money::EUR(0);
    }

    /**
     * @param $competingOffers
     * @param $state
     * @return Money
     */
    private function getStrategyPriceOne($competingOffers, $state)
    {
        $competingOffer = $this->getFirstCompetingByState($competingOffers, $state);

        return !is_null($competingOffer) ? $competingOffer->getPrice()->subtract(Money::EUR(1)) : null;
    }

    /**
     * @param $competingOffers
     * @param $state
     * @return Money
     */
    private function getStrategyPriceTwo($competingOffers, $state)
    {
        if ($state < Product::NEW_STATE) {
            $competingOffer = $this->getFirstCompetingByState($competingOffers, $state + 1);

            return !is_null($competingOffer) ? $competingOffer->getPrice()->subtract(Money::EUR(100)) : null;
        }

        return Money::EUR(0);
    }


    /**
     * @param $competingOffers Collection
     * @param $state
     * @return CompetingOffer|mixed|null
     */
    private function getFirstCompetingByState($competingOffers, $state)
    {
        /**
         * @var CompetingOffer $competingOffer
         */
        foreach ($competingOffers as $competingOffer) {
            if ($competingOffer->getState() === $state) {
                return $competingOffer;
            }
        }

        return null;
    }
}