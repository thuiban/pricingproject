<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200715190614 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP INDEX IDX_84F10EF24584665A');
        $this->addSql('CREATE TEMPORARY TABLE __temp__competing_offer AS SELECT id, product_id, seller_name, price, state FROM competing_offer');
        $this->addSql('DROP TABLE competing_offer');
        $this->addSql('CREATE TABLE competing_offer (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, product_id INTEGER DEFAULT NULL, seller_name VARCHAR(255) NOT NULL COLLATE BINARY, state INTEGER NOT NULL, price VARCHAR(255) NOT NULL --(DC2Type:money)
        , CONSTRAINT FK_84F10EF24584665A FOREIGN KEY (product_id) REFERENCES product (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO competing_offer (id, product_id, seller_name, price, state) SELECT id, product_id, seller_name, price, state FROM __temp__competing_offer');
        $this->addSql('DROP TABLE __temp__competing_offer');
        $this->addSql('CREATE INDEX IDX_84F10EF24584665A ON competing_offer (product_id)');
        $this->addSql('DROP INDEX IDX_29D6873E4584665A');
        $this->addSql('CREATE TEMPORARY TABLE __temp__offer AS SELECT id, product_id, price, state FROM offer');
        $this->addSql('DROP TABLE offer');
        $this->addSql('CREATE TABLE offer (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, product_id INTEGER DEFAULT NULL, state INTEGER NOT NULL, price VARCHAR(255) NOT NULL --(DC2Type:money)
        , minimum_price VARCHAR(255) NOT NULL --(DC2Type:money)
        , CONSTRAINT FK_29D6873E4584665A FOREIGN KEY (product_id) REFERENCES product (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO offer (id, product_id, price, state) SELECT id, product_id, price, state FROM __temp__offer');
        $this->addSql('DROP TABLE __temp__offer');
        $this->addSql('CREATE INDEX IDX_29D6873E4584665A ON offer (product_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP INDEX IDX_84F10EF24584665A');
        $this->addSql('CREATE TEMPORARY TABLE __temp__competing_offer AS SELECT id, product_id, seller_name, price, state FROM competing_offer');
        $this->addSql('DROP TABLE competing_offer');
        $this->addSql('CREATE TABLE competing_offer (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, product_id INTEGER DEFAULT NULL, seller_name VARCHAR(255) NOT NULL, state INTEGER NOT NULL, price DOUBLE PRECISION NOT NULL)');
        $this->addSql('INSERT INTO competing_offer (id, product_id, seller_name, price, state) SELECT id, product_id, seller_name, price, state FROM __temp__competing_offer');
        $this->addSql('DROP TABLE __temp__competing_offer');
        $this->addSql('CREATE INDEX IDX_84F10EF24584665A ON competing_offer (product_id)');
        $this->addSql('DROP INDEX IDX_29D6873E4584665A');
        $this->addSql('CREATE TEMPORARY TABLE __temp__offer AS SELECT id, product_id, price, state FROM offer');
        $this->addSql('DROP TABLE offer');
        $this->addSql('CREATE TABLE offer (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, product_id INTEGER DEFAULT NULL, state INTEGER NOT NULL, price DOUBLE PRECISION NOT NULL)');
        $this->addSql('INSERT INTO offer (id, product_id, price, state) SELECT id, product_id, price, state FROM __temp__offer');
        $this->addSql('DROP TABLE __temp__offer');
        $this->addSql('CREATE INDEX IDX_29D6873E4584665A ON offer (product_id)');
    }
}
