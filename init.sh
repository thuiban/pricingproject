#!/usr/bin/env bash


echo -n "Install project: "
 composer install
 yarn install
yarn encore production

echo -n "Init database: "
php bin/console doctrine:database:create
php bin/console doctrine:migrations:migrate --no-interaction
php bin/console doctrine:fixtures:load --env=dev -n

echo -n "Installation OK"
